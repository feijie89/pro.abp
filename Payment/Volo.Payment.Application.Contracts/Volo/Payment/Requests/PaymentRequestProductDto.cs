﻿using System;
using System.Collections.Generic;
using Volo.Abp.Data;

namespace Volo.Payment.Requests
{
    [Serializable]
	public class PaymentRequestProductDto : IHasExtraProperties
	{
		public Guid PaymentRequestId { get; set; }

		public string Code { get; set; }

		public string Name { get; set; }

		public float UnitPrice { get; set; }

		public int Count { get; set; }

		public float TotalPrice { get; set; }

		public Dictionary<string, object> ExtraProperties { get; protected set; }

		public PaymentRequestProductDto()
		{
			this.ExtraProperties = new Dictionary<string, object>();
		}
	}
}
