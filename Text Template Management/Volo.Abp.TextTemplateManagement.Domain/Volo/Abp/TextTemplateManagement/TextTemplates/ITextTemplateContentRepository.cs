﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
	public interface ITextTemplateContentRepository : IReadOnlyBasicRepository<TextTemplateContent, Guid>, IReadOnlyBasicRepository<TextTemplateContent>, IBasicRepository<TextTemplateContent>, IBasicRepository<TextTemplateContent, Guid>, IRepository
	{
		Task<TextTemplateContent> GetAsync(string name, string cultureName = null);

		Task<TextTemplateContent> FindAsync(string name, string cultureName = null);
	}
}
