﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.LeptonTheme.Management;

namespace Volo.Abp.LeptonTheme
{
    [RemoteService(true, Name = "LeptonThemeManagement")]
	[Route("api/lepton-theme-management/settings")]
	[Area("leptonThemeManagement")]
	public class LeptonThemeSettingsController : AbpController, ILeptonThemeSettingsAppService, IApplicationService, IRemoteService
	{
		private readonly ILeptonThemeSettingsAppService service;

		public LeptonThemeSettingsController(ILeptonThemeSettingsAppService leptonThemeSettingsAppService)
		{
			this.service = leptonThemeSettingsAppService;
		}

		[HttpGet]
		public Task<LeptonThemeSettingsDto> GetAsync()
		{
			return this.service.GetAsync();
		}

		[HttpPut]
		public Task UpdateAsync(UpdateLeptonThemeSettingsDto input)
		{
			return this.service.UpdateAsync(input);
		}
	}
}
