﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.LeptonTheme.Management.Permissions;
using Volo.Abp.SettingManagement;

namespace Volo.Abp.LeptonTheme.Management
{
    [Authorize(LeptonThemeManagementPermissions.Settings.SettingsGroup)]
	public class LeptonThemeSettingsAppService : ApplicationService, ILeptonThemeSettingsAppService, IApplicationService, IRemoteService
	{
		protected ISettingManager SettingManager { get; }

		public LeptonThemeSettingsAppService(ISettingManager settingManager)
		{
			SettingManager = settingManager;
		}

		public async Task<LeptonThemeSettingsDto> GetAsync()
		{
			MenuPlacement menuPlacement;
			Enum.TryParse<MenuPlacement>(await UserSettingManagerExtensions.GetOrNullForCurrentUserAsync(this.SettingManager, "Volo.Abp.LeptonTheme.Layout.MenuPlacement", true), out menuPlacement);
			MenuStatus menuStatus;
			Enum.TryParse<MenuStatus>(await UserSettingManagerExtensions.GetOrNullForCurrentUserAsync(this.SettingManager, "Volo.Abp.LeptonTheme.Layout.MenuStatus", true), out menuStatus);
			LeptonStyle style;
			Enum.TryParse<LeptonStyle>(await UserSettingManagerExtensions.GetOrNullForCurrentUserAsync(this.SettingManager, "Volo.Abp.LeptonTheme.Style", true), out style);
			var leptonThemeSettingsDto = new LeptonThemeSettingsDto();
			leptonThemeSettingsDto.BoxedLayout = Convert.ToBoolean(await UserSettingManagerExtensions.GetOrNullForCurrentUserAsync(this.SettingManager, "Volo.Abp.LeptonTheme.Layout.Boxed", true));
			leptonThemeSettingsDto.MenuPlacement = menuPlacement;
			leptonThemeSettingsDto.MenuStatus = menuStatus;
			leptonThemeSettingsDto.Style = style;
			return leptonThemeSettingsDto;
		}

		public async Task UpdateAsync(UpdateLeptonThemeSettingsDto input)
		{
			await this.SettingManager.SetForCurrentTenantAsync("Volo.Abp.LeptonTheme.Layout.Boxed", input.BoxedLayout.ToString());
			await this.SettingManager.SetForCurrentTenantAsync("Volo.Abp.LeptonTheme.Layout.MenuPlacement", input.MenuPlacement.ToString());
			await this.SettingManager.SetForCurrentTenantAsync("Volo.Abp.LeptonTheme.Layout.MenuStatus", input.MenuStatus.ToString());
			await this.SettingManager.SetForCurrentTenantAsync("Volo.Abp.LeptonTheme.Style", input.Style.ToString());
		}
	}
}
