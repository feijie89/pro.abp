﻿using Volo.Abp.Reflection;

namespace Volo.Abp.LanguageManagement
{
    public class LanguageManagementPermissions
	{
		public static string[] GetAll()
		{
			return ReflectionHelper.GetPublicConstantsRecursively(typeof(LanguageManagementPermissions));
		}

		public const string GroupName = "LanguageManagement";

		public class LanguageTexts
		{
			public const string Default = "LanguageManagement.LanguageTexts";

			public const string Edit = "LanguageManagement.LanguageTexts.Edit";
		}

		public class Languages
		{
			public const string Default = "LanguageManagement.Languages";

			public const string Edit = "LanguageManagement.Languages.Edit";

			public const string Create = "LanguageManagement.Languages.Create";

			public const string ChangeDefault = "LanguageManagement.Languages.ChangeDefault";

			public const string Delete = "LanguageManagement.Languages.Delete";
		}
	}
}
