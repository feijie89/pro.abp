﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.LanguageManagement.Dto;

namespace Volo.Abp.LanguageManagement
{
    [Route("api/language-management/language-texts")]
	[ControllerName("LanguageTexts")]
	[Area("languageManagement")]
	[RemoteService(true, Name = LanguageManagementRemoteServiceConsts.RemoteServiceName)]
	public class LanguageTextController : AbpController, IRemoteService, IApplicationService, ILanguageTextAppService
	{
		protected ILanguageTextAppService LanguageTextAppService { get; }

		public LanguageTextController(ILanguageTextAppService languageTextAppService)
		{
			this.LanguageTextAppService = languageTextAppService;
		}

		[HttpGet]
		public virtual Task<PagedResultDto<LanguageTextDto>> GetListAsync(GetLanguagesTextsInput input)
		{
			return this.LanguageTextAppService.GetListAsync(input);
		}

		[Route("{resourceName}/{cultureName}/{name}")]
		[HttpGet]
		public virtual Task<LanguageTextDto> GetAsync(string resourceName, string cultureName, string name, string baseCultureName)
		{
			return this.LanguageTextAppService.GetAsync(resourceName, cultureName, name, baseCultureName);
		}

		[Route("{resourceName}/{cultureName}/{name}")]
		[HttpPut]
		public virtual async Task UpdateAsync(string resourceName, string cultureName, string name, string value)
		{
			await this.LanguageTextAppService.UpdateAsync(resourceName, cultureName, name, value);
		}

		[Route("{resourceName}/{cultureName}/{name}/restore")]
		[HttpPut]
		public virtual async Task RestoreToDefaultAsync(string resourceName, string cultureName, string name)
		{
			await this.LanguageTextAppService.RestoreToDefaultAsync(resourceName, cultureName, name);
		}
	}
}
