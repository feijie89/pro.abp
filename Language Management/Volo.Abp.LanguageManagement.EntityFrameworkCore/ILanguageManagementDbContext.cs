﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace Volo.Abp.LanguageManagement.EntityFrameworkCore
{
	[ConnectionStringName("AbpLanguageManagement")]
	public interface ILanguageManagementDbContext : IInfrastructure<IServiceProvider>, IResettableService, IDbContextPoolable, IDbSetCache, IDbContextDependencies, IDisposable, IEfCoreDbContext
	{
		DbSet<Language> Languages { get; set; }

		DbSet<LanguageText> LanguageTexts { get; set; }
	}
}
