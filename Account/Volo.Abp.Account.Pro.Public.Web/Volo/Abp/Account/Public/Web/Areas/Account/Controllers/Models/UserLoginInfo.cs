﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Auditing;
using Volo.Abp.Identity;
using Volo.Abp.Validation;

namespace Volo.Abp.Account.Public.Web.Areas.Account.Controllers.Models
{
	public class UserLoginInfo
	{
		[StringLength(255)]
		[Required]
		public string UserNameOrEmailAddress { get; set; }

		[DynamicStringLength(typeof(IdentityUserConsts), "MaxPasswordLength", null)]
		[DisableAuditing]
		[DataType(DataType.Password)]
		[Required]
		public string Password { get; set; }

		public bool RememberMe { get; set; }

		public Guid? TenanId { get; set; }
	}
}
